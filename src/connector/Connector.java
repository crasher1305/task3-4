package connector;

import exceptions.SerializableException;
import figures.ListOfFigures;

import java.io.*;

/**
 *class is intended for writing and reading boxes from a file
 * @author Churakov
 * @version 1.0
 */
public class Connector {
    /**
     * @param FILEBIN - bin file name
     */
    private static final String FILEBIN =
            System.getProperty("user.dir") + File.separator + "figures.bin";

    public Connector() {
    }

    /**
     * method for writing boxes to file
     * @param box
     */
    public void saveToBin(ListOfFigures.Box box) throws SerializableException {
        try {
            FileOutputStream fos = new FileOutputStream(FILEBIN);
            ObjectOutputStream oos = new ObjectOutputStream(fos);

            oos.writeObject(box);
            oos.flush();
            oos.close();
        } catch (FileNotFoundException e) {
            throw new SerializableException(e);
        } catch (IOException e) {
            throw new SerializableException(e);
        }
    }

    /**
     * method for reading boxes from file
     * @return box
     */
    public ListOfFigures.Box loadFromBin() throws SerializableException {
        try {
            FileInputStream fis = new FileInputStream(FILEBIN);
            ObjectInputStream ois = new ObjectInputStream(fis);
            ListOfFigures.Box box = (ListOfFigures.Box) ois.readObject();
            ois.close();

            return box;
        } catch (FileNotFoundException e) {
            throw new SerializableException(e);
        } catch (IOException e) {
            throw new SerializableException(e);
        } catch (ClassNotFoundException e) {
            throw new SerializableException(e);
        }
    }
}
