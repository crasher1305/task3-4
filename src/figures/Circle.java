package figures;

import exceptions.IncorrectDataException;

import java.util.Objects;

public abstract class Circle extends Figure {
    double radius;
    private static final long serialVersionUID = 4L;

    public Circle() {
    }

    public Circle(double radius) throws IncorrectDataException {
        if (radius < 0) {
            throw new IncorrectDataException("circle radius < 0");
        }
        this.radius = radius;

    }

    /**
     * copy constructor
     * @param circle - figure for copy
     */
    public Circle(Circle circle) {
        this.radius = circle.radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) throws IncorrectDataException {
        if (radius < 0) {
            throw new IncorrectDataException("circle radius < 0");
        }
        this.radius = radius;
    }

    /**
     * checks if the figures are equal
     * @param o - figure for compare
     * @return compare result
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Circle circle = (Circle) o;
        return Double.compare(circle.radius, radius) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(radius);
    }

    @Override
    public double getSquareFigure() {
        return Math.PI * Math.pow(radius, 2);
    }

    @Override
    public double getPerimeterFigure() {
        return 2 * Math.PI * radius;
    }
}
