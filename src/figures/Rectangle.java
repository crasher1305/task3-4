package figures;

import exceptions.IncorrectDataException;

import java.io.Serializable;
import java.util.Objects;

public abstract class Rectangle extends Figure{
    private static final long serialVersionUID = 6L;

    double sideA;
    double sideB;

    public Rectangle() {
    }

    public Rectangle(double sideA, double sideB) throws IncorrectDataException {
        if (sideA < 0 || sideB < 0) {
            throw new IncorrectDataException("rectangle side < 0");
        }

        this.sideA = sideA;
        this.sideB = sideB;

    }

    public Rectangle(Rectangle rectangle) {
        this.sideA = rectangle.sideA;
        this.sideB = rectangle.sideB;
    }

    public double getSideA() {
        return sideA;
    }

    public void setSideA(double sideA) throws IncorrectDataException {
        if (sideA < 0) {
            throw new IncorrectDataException("rectangle side A < 0");
        }
        this.sideA = sideA;

    }

    public double getSideB() {
        return sideB;
    }

    public void setSideB(double sideB) throws IncorrectDataException {
        if (sideB < 0) {
            throw new IncorrectDataException("rectangle side B < 0");
        }
        this.sideB = sideB;
    }

    /**
     * checks if the figures are equal
     * @param o - figure for compare
     * @return compare result
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rectangle rectangle = (Rectangle) o;
        return Double.compare(rectangle.sideA, sideA) == 0 &&
                Double.compare(rectangle.sideB, sideB) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(sideA, sideB);
    }

    @Override
    public double getSquareFigure() {
        return sideA * sideB;
    }

    @Override
    public double getPerimeterFigure() {
        return (sideA + sideB) * 2;
    }
}
