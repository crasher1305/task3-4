package figures;

import exceptions.IncorrectDataException;
import materials.Membrane;

public class MembraneSquare extends Square implements Membrane {
    private static final long serialVersionUID = 11L;
    public MembraneSquare() {
    }

    public MembraneSquare(double side) throws IncorrectDataException {
        super(side);
    }

    public MembraneSquare(Square square) {
        super(square);
    }

    /**
     * this method looking for a square with
     * the same color and similar side
     * @return true - if the figure is found; else - if figure not found
     */
    @Override
    public boolean equalsSimilarFigure(Figure figure) {

        MembraneSquare desiredSquare = (MembraneSquare) figure;
        if ((Math.abs(desiredSquare.side - this.side)) <= 0.5) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Created String for a shape
     * @return figure characteristics
     */
    @Override
    public String toString() {
        return "MembraneSquare{" +
                "side = " + side +
                ", square = " + getSquareFigure() +
                ", perimeter = " + getPerimeterFigure() +
                '}';
    }
}
