package figures;

import exceptions.IncorrectDataException;
import materials.Color;
import materials.Paper;

import java.util.Objects;

public class PaperSquare extends Square implements Paper{
    private static final long serialVersionUID = 15L;
    Color color = null;

    public PaperSquare() {
    }

    public PaperSquare(double side) throws IncorrectDataException {
        super(side);
    }

    /**
     cutting constructor
     * @param square - the figure that we cut
     * @param figure - the figure from which we cut
     */
    public PaperSquare(Square square, Figure figure) {
        super(square);
        this.color = ((Paper)figure).checkColor();
    }

    @Override
    public boolean checkPainting() {
        if (color != null) {
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public void paintingFigure(Color color) {
        if (checkPainting() == false) {
            this.color = color;
        }
    }

    @Override
    public Color checkColor() {
        if (checkPainting() == true) {
            return color;
        }
        else {
            return null;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PaperSquare that = (PaperSquare) o;
        return color == that.color;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), color);
    }

    /**
     * this method looking for a square with
     * the same color and similar side
     * @return true - if the figure is found; else - if figure not found
     */
    @Override
    public boolean equalsSimilarFigure(Figure figure) {
        PaperSquare desiredSquare = (PaperSquare) figure;
        if ((Math.abs(desiredSquare.side - this.side)) <= 0.5 && this.color == desiredSquare.color) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Created String for a shape
     * @return figure characteristics
     */
    @Override
    public String toString() {
        if (checkPainting()) {
            return "PaperSquare{" +
                    "side = " + side +
                    ", square = " + getSquareFigure() +
                    ", perimeter = " + getPerimeterFigure() +
                    ", color = " + color +
                    '}';
        }
        else {
            return "PaperSquare{" +
                    "side = " + side +
                    ", square = " + getSquareFigure() +
                    ", perimeter = " + getPerimeterFigure() +
                    ", square is not painted " +
                    '}';
        }
    }
}
