package figures;

import exceptions.IncorrectDataException;
import materials.Color;
import materials.Paper;

import java.util.Objects;

public class PaperEquilTriangle extends EquilateralTriangle implements Paper{
    private static final long serialVersionUID = 13L;
    Color color = null;

    public PaperEquilTriangle() {
    }

    public PaperEquilTriangle(double side) throws IncorrectDataException {
        super(side);
    }

    /**
     cutting constructor
     * @param equilateralTriangle - the figure that we cut
     * @param figure - the figure from which we cut
     */
    public PaperEquilTriangle(EquilateralTriangle equilateralTriangle, Figure figure) {
        super(equilateralTriangle);
        this.color = ((Paper)figure).checkColor();
    }

    @Override
    public boolean checkPainting() {
        if (color != null) {
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public void paintingFigure(Color color) {
        if (checkPainting() == false) {
            this.color = color;
        }
    }

    @Override
    public Color checkColor() {
        if (checkPainting() == true) {
            return color;
        }
        else {
            return null;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PaperEquilTriangle that = (PaperEquilTriangle) o;
        return color == that.color;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), color);
    }

    /**
     * this method looking for a triangle with similar side
     * @return true - if the figure is found; else - if figure not found
     */
    @Override
    public boolean equalsSimilarFigure(Figure figure) {
        PaperEquilTriangle desiredTriangle = (PaperEquilTriangle) figure;
        if ((Math.abs(desiredTriangle.side - this.side)) <= 0.5  && this.color == desiredTriangle.color) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Created String for a shape
     * @return figure characteristics
     */
    @Override
    public String toString() {
        if (checkPainting()) {
            return "PaperEquilateralTriangle{" +
                    "side = " + side +
                    ", square = " + getSquareFigure() +
                    ", perimeter = " + getPerimeterFigure() +
                    ", color = " + color +
                    '}';
        }
        else {
            return "PaperEquilateralTriangle{" +
                    "side=" + side +
                    ", square = " + getSquareFigure() +
                    ", perimeter = " + getPerimeterFigure() +
                    ", equilateral triangle is no painted " +
                    '}';
        }
    }
}
