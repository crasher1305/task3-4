package figures;

import exceptions.IncorrectDataException;

import java.util.Objects;

public abstract class EquilateralTriangle extends Figure {
    double side;
    private static final long serialVersionUID = 5L;

    public EquilateralTriangle() {
    }

    public EquilateralTriangle(double side) throws IncorrectDataException {
        if (side < 0) {
            throw new IncorrectDataException("triangle side < 0");
        }

        this.side = side;
    }

    /**
     * copy constructor
     * @param equilateralTriangle - figure for copy
     */
    public EquilateralTriangle(EquilateralTriangle equilateralTriangle) {
        this.side = equilateralTriangle.side;
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) throws IncorrectDataException {
        if (side < 0) {
            throw new IncorrectDataException("triangle side < 0");
        }

        this.side = side;
    }

    /**
     * checks if the figures are equal
     * @param o - figure for compare
     * @return compare result
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EquilateralTriangle that = (EquilateralTriangle) o;
        return Double.compare(that.side, side) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(side);
    }

    @Override
    public double getSquareFigure() {
        return (Math.pow(side, 2) * Math.sqrt(3)) / 4;
    }

    @Override
    public double getPerimeterFigure() {
        return 3 * side;
    }
}
