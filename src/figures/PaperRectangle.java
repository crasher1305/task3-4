package figures;

import exceptions.IncorrectDataException;
import materials.Color;
import materials.Paper;

import java.util.Objects;

public class PaperRectangle extends Rectangle implements Paper {
    private static final long serialVersionUID = 14L;
    Color color = null;
    static int testStatic = 1234567;
    transient int testTransient;

    public PaperRectangle() {
    }

    public PaperRectangle(double sideA, double sideB) throws IncorrectDataException {
        super(sideA, sideB);
    }

    public PaperRectangle(double sideA, double sideB, int testTransient) throws IncorrectDataException {
        super(sideA, sideB);
        this.testTransient = testTransient;
    }

    public PaperRectangle(Rectangle rectangle, Color color, int testTransient) {
        super(rectangle);
        this.color = color;
        this.testTransient = testTransient;
    }

    /**
     cutting constructor
     * @param rectangle - the figure that we cut
     * @param figure - the figure from which we cut
     */
    public PaperRectangle(Rectangle rectangle, Figure figure) {
        super(rectangle);
        this.color = ((Paper)figure).checkColor();
    }

    @Override
    public boolean checkPainting() {
        if (color != null) {
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public void paintingFigure(Color color) {
        if (checkPainting() == false) {
            this.color = color;
        }
    }

    @Override
    public Color checkColor() {
        if (checkPainting() == true) {
            return color;
        }
        else {
            return null;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PaperRectangle that = (PaperRectangle) o;
        return color == that.color;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), color);
    }

    /**
     * this method looking for a rectangle with
     * the same color and similar sides
     * @return true - if the figure is found; else - if figure not found
     */
    @Override
    public boolean equalsSimilarFigure(Figure figure) {
        PaperRectangle desiredRectangle = (PaperRectangle) figure;
        if ((Math.abs(desiredRectangle.sideA - this.sideA)) <= 0.5 &&
                (Math.abs(desiredRectangle.sideB - this.sideB)) <= 0.5 &&
                this.color == desiredRectangle.color) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Created String for a shape
     * @return figure characteristics
     */
    @Override
    public String toString() {
        if(checkPainting()) {
            return "PaperRectangle{" +
                    "sideA=" + sideA +
                    ", sideB=" + sideB +
                    ", square = " + getSquareFigure() +
                    ", perimeter = " + getPerimeterFigure() +
                    ", color = " + color +
                    ", testStatic = " + testStatic +
                    ", testTransient = " + testTransient +
                    '}';
        }
        else {
            return "PaperRectangle{" +
                    "sideA=" + sideA +
                    ", sideB=" + sideB +
                    ", square = " + getSquareFigure() +
                    ", perimeter = " + getPerimeterFigure() +
                    ", testStatic = " + testStatic +
                    ", testTransient = " + testTransient +
                    ", rectangle is not painted " +
                    '}';
        }
    }
}