package figures;

import exceptions.IncorrectDataException;
import materials.Membrane;


public class MembraneEquilTriangle extends EquilateralTriangle implements Membrane{
    private static final long serialVersionUID = 9L;
    public MembraneEquilTriangle() {
    }

    public MembraneEquilTriangle(double side) throws IncorrectDataException {
        super(side);
    }

    public MembraneEquilTriangle(EquilateralTriangle equilateralTriangle) {
        super(equilateralTriangle);
    }

    /**
     * this method looking for a triangle with similar side
     * @return true - if the figure is found; else - if figure not found
     */
    @Override
    public boolean equalsSimilarFigure(Figure figure) {
        MembraneEquilTriangle desiredTriangle = (MembraneEquilTriangle) figure;
        if ((Math.abs(desiredTriangle.side - this.side)) <= 0.5) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Created String for a shape
     * @return figure characteristics
     */
    @Override
    public String toString() {
        return "MembraneEquilTriangle{" +
                "side=" + side +
                ", square = " + getSquareFigure() +
                ", perimeter = " + getPerimeterFigure() +
                '}';
    }
}
