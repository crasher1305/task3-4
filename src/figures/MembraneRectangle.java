package figures;

import exceptions.IncorrectDataException;
import materials.Membrane;

public class MembraneRectangle extends Rectangle implements Membrane {
    private static final long serialVersionUID = 10L;
    public MembraneRectangle() {
    }

    public MembraneRectangle(double sideA, double sideB) throws IncorrectDataException {
        super(sideA, sideB);
    }

    public MembraneRectangle(Rectangle rectangle) {
        super(rectangle);
    }

    /**
     * this method looking for a rectangle with similar sides
     * @return true - if the figure is found; else - if figure not found
     */
    @Override
    public boolean equalsSimilarFigure(Figure figure) {
        MembraneRectangle desiredRectangle = (MembraneRectangle) figure;
        if ((Math.abs(desiredRectangle.sideA - this.sideA)) <= 0.5 &&
                (Math.abs(desiredRectangle.sideB - this.sideB)) <= 0.5) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Created String for a shape
     * @return figure characteristics
     */
    @Override
    public String toString() {
        return "MembraneRectangle{" +
                "sideA=" + sideA +
                ", sideB=" + sideB +
                ", square = " + getSquareFigure() +
                ", perimeter = " + getPerimeterFigure() +
                '}';
    }
}
