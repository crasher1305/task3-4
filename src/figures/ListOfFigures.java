package figures;

import connector.Connector;
import exceptions.IncorrectDataException;
import exceptions.SerializableException;
import materials.Color;
import materials.Membrane;
import materials.Paper;

import java.io.Serializable;
import java.util.ArrayList;


/**
 *class contains a collection of figures and a box with various methods
 * @author Churakov
 * @version 1.0
 */
public class ListOfFigures implements Serializable{
    /**
     * this class contains a collection of shapes before being boxed
     * @param figures - collection of shapes
     * @param box - folding box
     * @param connector - for writing and reading boxes from a file
     */
    transient ArrayList<Figure> figures = new ArrayList<>();
    Box box = new Box();
    static Connector connector;
    private static final long serialVersionUID = 1L;

    static {
        connector = new Connector();
    }

    public ListOfFigures(ArrayList<Figure> figures, Box box) {
        this.figures = figures;
        this.box = box;
    }

    /**
     * adding several shapes to a list
     */
    public ListOfFigures() {
    }

    /**
     * method for load box from bim file
     */
    public void loadBoxFromBin() throws SerializableException {
        box = connector.loadFromBin();
    }

    /**
     * method for recording box in bin file
     */
    public void saveBoxInBin() throws SerializableException {
        if (box != null) {
            connector.saveToBin(box);
        }
    }

    /**
     * @return collection of figures
     */
    public ArrayList<Figure> getFigures() {
        return figures;
    }

    public void setFigures(ArrayList<Figure> figures) {
        this.figures = figures;
    }

    /**
     * @return folding box
     */
    public Box getBox() {
        return box;
    }

    public void setBox(Box box) {
        this.box = box;
    }

    /**
     * this method check that the desired shape is in the collection;
     * check that the material of the desired shape is paper;
     * call a method which will color a figure;
     * @param numPaintingFigure - the number of figures in the collection that we paint
     * @param color - the color we want to color the figure
     */
    public void getFigurePainting(int numPaintingFigure, Color color) {
        if (numPaintingFigure <= figures.size()) {
            if (figures.get(numPaintingFigure - 1) instanceof Paper) {
                ((Paper) figures.get(numPaintingFigure - 1)).paintingFigure(color);
            }
        }
    }

    public class Box implements Serializable{
        /**
         * class folding box
         * @param figuresInBox - collection of figures in a box
         */
        ArrayList<Figure> figuresInBox = new ArrayList<>();
        private static final long serialVersionUID = 2L;

        public Box() {
        }

        public ArrayList<Figure> getFiguresInBox() {
            return figuresInBox;
        }

        public void setFiguresInBox(ArrayList<Figure> figuresInBox) {
            this.figuresInBox = figuresInBox;
        }

        /**
         * this method
         * @return num of figures in a box
         */
        public int genNumFiguresInBox() {
            return figuresInBox.size();
        }

        /**
         * this method checks if there is a place in the box;
         * also check if the same figure is in the box
         * and add figure in the box
         * @param figure - Figure we want to put in the box
         */
        public void addFigureInBox(Figure figure) {
            if (figuresInBox.size() <= 20 ) {
                for (int i = 0; i < figuresInBox.size(); i++) {
                    if (figure.equals(figuresInBox.get(i)) == true){
                        return;
                    }
                }
                figuresInBox.add(figure);
            }
        }

        /**
         * this method check that the desired shape is in the box;
         * @param numFigure - number of figore in a box
         * @return figure by number
         */
        public Figure infoFigureInBox(int numFigure) {
            if (numFigure - 1 <= figuresInBox.size()) {
                return figuresInBox.get(numFigure - 1);
            }
            return null;
        }

        /**
         * this method check that the desired shape is in the box
         * and extraction it by number
         * @param numFigure - number figure we want to extraction
         */
        public void figureExtraction(int numFigure) {
            if (numFigure - 1 <= figuresInBox.size()) {
                figuresInBox.remove(numFigure - 1);
            }
        }

        /**
         * this method check that the desired shape is in the box
         * and replace it
         * @param numReplace - number figure we want to replace
         * @param figure - Figure we want to put in the box
         */
        public void replaceFigure(Figure figure, int numReplace) {
            if (numReplace - 1 <= figuresInBox.size()) {
                figuresInBox.set(numReplace - 1, figure);
            }
        }

        /**
         * this method looking for figures of the same class
         * and calls a method that compares shapes
         * @param similarFigure - the figure we are looking for
         * @return figure - if the figure is found; null - if figure not found
         */
        public Figure findSimilarFigure(Figure similarFigure) {
            for (int i = 0; i < figuresInBox.size(); i++) {
                if (figuresInBox.get(i).getClass() == similarFigure.getClass()) {
                    if (figuresInBox.get(i).equalsSimilarFigure(similarFigure) == true) {
                        return  figuresInBox.get(i);
                    }
                }
            }
            return null;
        }

        /**
         * Calculation of the square of all figures
         * @return the square of all figures.
         */
        public double getAllFiguresSquare() {

            double allFiguresSquare = 0;
            for (int i = 0; i < figuresInBox.size(); i++) {
                allFiguresSquare += figuresInBox.get(i).getSquareFigure();
            }
            return allFiguresSquare;
        }

        /**
         * Calculation of the perimeter of all figures
         * @return the perimeter of all figures.
         */
        public double getAllFiguresPerimeter() {
            double allFiguresPerimeter = 0;
            for (int i = 0; i < figuresInBox.size(); i++) {
                allFiguresPerimeter += figuresInBox.get(i).getPerimeterFigure();
            }
            return allFiguresPerimeter;
        }

        /**
         * Extraction of all circles
         * @return extracted circles
         */
        public ArrayList<Figure> getAllCircles() {
            ArrayList<Figure> figures = new ArrayList<>();
            for (int i = 0; i < figuresInBox.size(); i++) {
                if (figuresInBox.get(i) instanceof Circle) {
                    figures.add(figuresInBox.get(i));
                    figuresInBox.remove(i);
                    i--;
                }
            }
            return figures;
        }

        /**
         * Extraction of all membrane figures
         * @return extracted membrane figures
         */
        public ArrayList<Figure> getAllMembraneFigures() {
            ArrayList<Figure> figures = new ArrayList<>();
            for (int i = 0; i < figuresInBox.size(); i++) {
                if (figuresInBox.get(i) instanceof Membrane) {
                    figures.add(figuresInBox.get(i));
                    figuresInBox.remove(i);
                    i--;
                }
            }
            return figures;
        }
    }
}