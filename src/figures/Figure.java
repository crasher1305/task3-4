package figures;

import java.io.Serializable;

/**
 * abstract class from which the figures are inherited
 * @author Churakov
 * @version 1.0
 */
public abstract class Figure implements Serializable {
    private static final long serialVersionUID = 3L;
    /**
     * Calculation of the area of a shape.
     *
     * @return the area of a share.
     */
    public abstract double getSquareFigure();

    /**
     * Calculation of the perimeter of a shape.
     *
     * @return the perimeter of a share.
     */
    public abstract double getPerimeterFigure();

    /**
     * search similar shape
     * @param figure figure for compare
     * @return compare result
     */
    public abstract boolean equalsSimilarFigure(Figure figure);
}
