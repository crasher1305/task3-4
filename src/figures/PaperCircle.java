package figures;

import exceptions.IncorrectDataException;
import materials.Color;
import materials.Paper;

import java.util.Objects;

public class PaperCircle extends Circle implements Paper{
    private static final long serialVersionUID = 12L;
    Color color = null;

    public PaperCircle() {
    }

    public PaperCircle(double radius) throws IncorrectDataException {
        super(radius);
    }

    /**
     cutting constructor
     * @param circle - the figure that we cut
     * @param figure - the figure from which we cut
     */
    public PaperCircle(Circle circle, Figure figure ) {
        super(circle);
        this.color = ((Paper)figure).checkColor();
    }

    @Override
    public boolean checkPainting() {
        if (color != null) {
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public void paintingFigure(Color color) {
        if (checkPainting() == false) {
            this.color = color;
        }
    }

    @Override
    public Color checkColor() {
        if (checkPainting() == true) {
            return color;
        }
        else {
            return null;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PaperCircle that = (PaperCircle) o;
        return color == that.color;
    }

    /**
     * this method looking for a circle with
     * the same color and similar radius
     * @return true - if the figure is found; else - if figure not found
     */
    @Override
    public boolean equalsSimilarFigure(Figure figure) {
        PaperCircle desiredCircle = (PaperCircle) figure;
        if ((Math.abs(desiredCircle.radius - this.radius)) <= 0.5 && this.color == desiredCircle.color) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Created String for a shape
     * @return figure characteristics
     */
    @Override
    public String toString() {
        if (checkPainting()) {
            return "PaperCircle{" +
                    "radius = " + radius +
                    ", square = " + getSquareFigure() +
                    ", perimeter = " + getPerimeterFigure() +
                    ", color = " + color +
                    '}';
        }
        else {
            return "PaperCircle{" +
                    "radius = " + radius +
                    ", square = " + getSquareFigure() +
                    ", perimeter = " + getPerimeterFigure() +
                    ", circle is not painted " +
                    '}';
        }


    }
}
