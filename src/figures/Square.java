package figures;

import exceptions.IncorrectDataException;

import java.util.Objects;

public abstract class Square extends Figure{
    private static final long serialVersionUID = 7L;

    double side;

    public Square() {
    }

    public Square(double side) throws IncorrectDataException {
        if (side < 0) {
            throw new IncorrectDataException("square side < 0");
        }
        this.side = side;
    }

    public Square(Square square) {
        this.side = square.side;
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) throws IncorrectDataException {
        if (side < 0) {
            throw new IncorrectDataException("square side < 0");
        }
        this.side = side;

    }

    /**
     * checks if the figures are equal
     * @param o - figure for compare
     * @return compare result
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Square square = (Square) o;
        return Double.compare(square.side, side) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(side);
    }

    @Override
    public double getSquareFigure() {
        return Math.pow(side, 2);
    }

    @Override
    public double getPerimeterFigure() {
        return side * 4;
    }
}
