package figures;

import exceptions.IncorrectDataException;
import materials.Membrane;

public class MembraneCircle extends Circle implements Membrane {
    private static final long serialVersionUID = 8L;

    public MembraneCircle() {
    }

    public MembraneCircle(double radius) throws IncorrectDataException {
        super(radius);
    }

    public MembraneCircle(Circle circle) {
        super(circle);
    }

    /**
     * this method looking for a circle with similar radius
     * @return true - if the figure is found; else - if figure not found
     */
    @Override
    public boolean equalsSimilarFigure(Figure figure) {
        MembraneCircle desiredCircle = (MembraneCircle) figure;
        if ((Math.abs(desiredCircle.radius - this.radius)) <= 0.5) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Created String for a shape
     * @return figure characteristics
     */
    @Override
    public String toString() {
        return "MembraneCircle{" +
                "radius = " + radius +
                ", square = " + getSquareFigure() +
                ", perimeter = " + getPerimeterFigure() +
                '}';
    }
}
