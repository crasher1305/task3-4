package exceptions;

import java.io.FileNotFoundException;
import java.io.IOException;

public class SerializableException extends Exception {
    public SerializableException(FileNotFoundException exception) {
        super(exception);
    }

    public SerializableException(IOException exception) {
        super(exception);
    }

    public SerializableException(ClassNotFoundException exception) {
        super(exception);
    }
}
