package materials;

public enum Color {
    RED, GREEN, BROWN, BLACK, YELLOW, ORANGE,
    PURPLE, VIOLET, GOLDEN;
}
