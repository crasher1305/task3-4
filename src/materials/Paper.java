package materials;

public interface Paper {
    /**
     * this method checks if the figure is painted
     * @return true - if the figure is painted; false - if the figure is not painted
     */
    boolean checkPainting();

    /**
     * this method paints a figure if if it is not already painted
     * @param color - figure color
     */
    void paintingFigure(Color color);

    /**
     * this method check figure color
     * @return figure color or null if figure is not painted
     */
    Color checkColor();
}
