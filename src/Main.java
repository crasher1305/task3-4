import exceptions.IncorrectDataException;
import exceptions.SerializableException;
import figures.*;

import java.util.ArrayList;

public class Main  {
    /**
     *  this method displays a collection of shapes
     *
     * @param figures - collection of shapes
     */
    public static void printCollectionFigures(ArrayList<Figure> figures) {
        for (int i = 0; i < figures.size(); i++){
            System.out.println(figures.get(i));
        }
        System.out.println("--------------------------------------------------------");
    }

    public static void main(String[] args) {

        ListOfFigures list1 = new ListOfFigures();
        try {
            list1.loadBoxFromBin();
        } catch (SerializableException e) {
            e.printStackTrace();
        }

        System.out.println("Figures in a box after load from bin file");
        ArrayList<Figure> figures1 = list1.getBox().getFiguresInBox();
        printCollectionFigures(figures1);

        System.out.println("Attempt to create a circle with radius < 0");
        PaperCircle paperCircle = null;
        try {
            paperCircle = new PaperCircle(-3);
        } catch (IncorrectDataException e) {
            e.printStackTrace();
        }

        System.out.println("Attempt to create a circle with radius > 0 and print");
        try {
            paperCircle = new PaperCircle(4);
        } catch (IncorrectDataException e) {
            e.printStackTrace();
        }
        System.out.println(paperCircle);

    }

}
